package connpool_test

import (
	"io"
	"log"
	"math/rand"
	"sync"
	"testing"
	"time"

	"gitlab.com/alexeysavchuk/connpool"
)

const (
	// min and max delays for dial and close operations.
	handleMinDelay = 1 * time.Millisecond
	handleMaxDelay = 10 * time.Millisecond

	// min and max delays for read and write operations.
	ioMinDelay = 10 * time.Millisecond
	ioMaxDelay = 100 * time.Millisecond

	idleTimeout = 100 * time.Millisecond
)

func handleDelay() {
	randomDelay := rand.Int63n(int64(handleMaxDelay - handleMinDelay))
	delayTime := time.Duration(randomDelay) + handleMinDelay
	time.Sleep(delayTime)
}

func ioDelay() {
	randomDelay := rand.Int63n(int64(ioMaxDelay - ioMinDelay))
	delayTime := time.Duration(randomDelay) + ioMinDelay
	time.Sleep(delayTime)
}

var (
	mu                  = &sync.Mutex{}
	numOpenConns        = 0
	maxOpenConns        = 2
	numIdleConnsPerHost = make(map[string]int)
)

type connState int

const (
	stateIdle connState = iota
	stateTransfer
	stateClosed
)

type mockConn struct {
	mu    sync.Mutex
	id    int
	state connState
	host  string
	data  []byte
}

func (c *mockConn) Read(b []byte) (n int, err error) {
	mu.Lock()
	if c.state != stateIdle {
		log.Panic("try to read from not idle connection")
	}
	c.state = stateTransfer
	mu.Unlock()

	n = copy(b, c.data)
	c.data = make([]byte, 1024)

	log.Printf("conn %d: start reading from %s", c.id, c.host)
	ioDelay()
	log.Printf("conn %d: end reading from %s", c.id, c.host)

	mu.Lock()
	c.state = stateIdle
	mu.Unlock()

	return n, nil
}

func (c *mockConn) Write(b []byte) (n int, err error) {
	mu.Lock()
	if c.state != stateIdle {
		log.Panic("try to write to not idle connection")
	}
	c.state = stateTransfer
	mu.Unlock()

	n = copy(c.data, b)
	c.data = c.data[:n]

	log.Printf("conn %d: start writing to %s", c.id, c.host)
	ioDelay()
	log.Printf("conn %d: end writing to %s", c.id, c.host)

	mu.Lock()
	c.state = stateIdle
	mu.Unlock()

	return n, nil
}

func (c *mockConn) Close() error {
	c.mu.Lock()
	if c.state != stateIdle {
		log.Panic("try to close not idle connection")
	}

	c.state = stateIdle
	c.mu.Unlock()

	mu.Lock()
	numOpenConns--
	numIdleConnsPerHost[c.host]--
	mu.Unlock()

	log.Printf("conn %d: start close", c.id)
	handleDelay()
	log.Printf("conn %d: end close", c.id)

	c.mu.Lock()
	c.state = stateClosed
	c.mu.Unlock()

	return nil
}

type mockDialer struct {
	sequence int
}

func (d *mockDialer) Dial(network, address string) (connpool.Conn, error) {
	mu.Lock()
	defer mu.Unlock()

	if numOpenConns >= maxOpenConns {
		log.Panic("open connection limit reached")
	}

	host := network + address

	numOpenConns++
	numIdleConnsPerHost[host]++

	conn := &mockConn{
		id:   d.sequence,
		host: host,
		data: make([]byte, 1024),
	}
	d.sequence++

	log.Printf("conn %d: start open", conn.id)
	handleDelay()
	log.Printf("conn %d: end open", conn.id)

	return conn, nil
}

var (
	_ connpool.OpenConn = (*mockConn)(nil)
	_ connpool.Dialer   = (*mockDialer)(nil)
)

const numHosts = 10

var hosts = [numHosts]string{
	"127.0.0.1:0",
	"127.0.0.1:1",
	"127.0.0.1:2",
	"127.0.0.1:3",
	"127.0.0.1:4",
	"127.0.0.1:5",
	"127.0.0.1:6",
	"127.0.0.1:7",
	"127.0.0.1:8",
	"127.0.0.1:9",
}

func testConnPool(numThreads int, t *testing.T) {
	log.SetOutput(io.Discard)
	dialer := &mockDialer{}

	pool := connpool.NewConnPool(dialer, idleTimeout, maxOpenConns)

	wg := &sync.WaitGroup{}
	wg.Add(numThreads)
	for range numThreads {
		go func() {
			network := "tcp"
			address := hosts[rand.Intn(numHosts)]

			conn, err := pool.Dial(network, address)
			if err != nil {
				log.Panic(err)
			}
			defer pool.Release(conn)

			data := []byte("hello")
			conn.Write(data)
			conn.Read(data)

			wg.Done()
		}()
	}
	wg.Wait()

	// wait for all connections to be closed
	time.Sleep(2*idleTimeout + handleMaxDelay)

	mu.Lock()
	if numOpenConns != 0 {
		t.Fatal("expected all connections to be closed")
	}
	mu.Unlock()
}

func TestConnPool10Threads(t *testing.T) {
	testConnPool(10, t)
}

func TestConnPool100Threads(t *testing.T) {
	testConnPool(100, t)
}

func TestConnPool1000Threads(t *testing.T) {
	testConnPool(1000, t)
}
