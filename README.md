## TCP/UDP Connection Pool

## Overview
This project aims to provide a reusable pool of TCP/UDP connections, allowing efficient management and reuse of established connections. Inspired by similar implementations for handling HTTP requests in standard libraries.

## Features
- [x] Fixed connection pool size
- [x] Close connection on idle timeout
- [ ] Pass context while dialing

## Usage
Connection pool with default ``Dialer``
```go
package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/alexeysavchuk/connpool"
)

func main() {
	pool := connpool.NewConnPool(
		// dialer responsible for opening connections
		connpool.DefaultDialer,
		// open connection idle timeout
		60*time.Second,
		// max open connections in pool
		10,
	)

	conn, err := pool.Dial("tcp", "127.0.0.1:80")
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to dial: %v", err)
		return
	}
	defer pool.Release(conn)

	// conn.Write(...)
	// conn.Read(...)
}
```

Connection pool with custom ``Dialer``
```go
package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"gitlab.com/alexeysavchuk/connpool"
)

type LogDialer struct {
}

func (d LogDialer) Dial(network, address string) (connpool.Conn, error) {
	log.Printf("dialing %s %s", network, address)

	conn, err := net.Dial(network, address)
	if err != nil {
		log.Printf("failed to dial: %v", err)

		return nil, err
	}
	log.Printf("dialing %s %s succeeded", network, address)

	return conn, nil
}

func main() {
	pool := connpool.NewConnPool(
		// dialer responsible for opening connections
		&LogDialer{},
		// open connection idle timeout
		60*time.Second,
		// max open connections in pool
		10,
	)

	conn, err := pool.Dial("tcp", "127.0.0.1:80")
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to dial: %v", err)
		return
	}
	defer pool.Release(conn)

	// conn.Write(...)
	// conn.Read(...)
}
```
