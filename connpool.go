package connpool

import (
	"context"
	"fmt"
	"net"
	"sync"
	"time"
)

type Conn interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	Close() (err error)
}

type OpenConn interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
}

type openConnWrapper struct {
	network       string
	address       string
	conn          Conn
	isIdle        bool
	cancelCleanup context.CancelFunc
}

func (w *openConnWrapper) Read(b []byte) (n int, err error) {
	return w.conn.Read(b)
}

func (w *openConnWrapper) Write(b []byte) (n int, err error) {
	return w.conn.Write(b)
}

type Dialer interface {
	Dial(network, address string) (Conn, error)
}

type defaultDialer struct {
}

func (d defaultDialer) Dial(network, address string) (Conn, error) {
	return net.Dial(network, address)
}

var DefaultDialer = defaultDialer{}

type connRequest struct {
	network  string
	address  string
	response chan *connResponse
}

type connResponse struct {
	conn *openConnWrapper
	err  error
}

type ConnPool struct {
	mu sync.Mutex

	dialer       Dialer
	maxOpenConns int
	idleTimeout  time.Duration

	numOpenConns int
	openConns    map[string][]*openConnWrapper
	waitingQueue []*connRequest
}

func NewConnPool(dialer Dialer, idleTimeout time.Duration, maxOpenConns int) *ConnPool {
	return &ConnPool{
		mu: sync.Mutex{},

		dialer:       dialer,
		idleTimeout:  idleTimeout,
		maxOpenConns: maxOpenConns,

		numOpenConns: 0,
		openConns:    make(map[string][]*openConnWrapper),
		waitingQueue: make([]*connRequest, 0),
	}
}

func (p *ConnPool) Dial(network, address string) (OpenConn, error) {
	req := &connRequest{
		network:  network,
		address:  address,
		response: make(chan *connResponse),
	}
	go p.dial(req)

	resp := <-req.response
	close(req.response)

	return resp.conn, resp.err
}

func (p *ConnPool) Release(conn OpenConn) {
	p.mu.Lock()
	defer p.mu.Unlock()

	connWrapper, ok := conn.(*openConnWrapper)
	if !ok {
		panic("try to release malformed connection")
	}

	host := connWrapper.network + connWrapper.address

	isReleased := false

	for _, c := range p.openConns[host] {
		if c == connWrapper {
			connWrapper.isIdle = true
			connWrapper.cancelCleanup = p.planCleanup(connWrapper)

			isReleased = true

			break
		}
	}

	if !isReleased {
		panic("try to release unknown connection")
	}

	for i, req := range p.waitingQueue {
		if req.network+req.address == host {
			p.waitingQueue = append(p.waitingQueue[:i], p.waitingQueue[i+1:]...)

			go p.dial(req)

			break
		}
	}
}

func (p *ConnPool) dial(req *connRequest) {
	conn, acquired, err := p.tryAcquire(req)
	if err != nil {
		req.response <- &connResponse{nil, err}

		return
	}

	if acquired {
		req.response <- &connResponse{conn, nil}

		return
	}

	p.enqueue(req)
}

func (p *ConnPool) tryAcquire(req *connRequest) (conn *openConnWrapper, acquired bool, err error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	host := req.network + req.address
	conns, ok := p.openConns[host]

	if ok {
		for _, conn := range conns {
			if !conn.isIdle {
				continue
			}

			conn.cancelCleanup()

			conn.isIdle = false
			conn.cancelCleanup = nil

			return conn, true, nil
		}
	}

	if p.numOpenConns < p.maxOpenConns {
		rawConn, err := p.dialer.Dial(req.network, req.address)
		if err != nil {
			return nil, false, err
		}

		conn = &openConnWrapper{
			network:       req.network,
			address:       req.address,
			conn:          rawConn,
			isIdle:        false,
			cancelCleanup: nil,
		}

		p.openConns[host] = append(conns, conn)
		p.numOpenConns++

		return conn, true, nil
	}

	return nil, false, nil
}

func (p *ConnPool) enqueue(req *connRequest) {
	p.mu.Lock()
	defer p.mu.Unlock()

	p.waitingQueue = append(p.waitingQueue, req)
}

func (p *ConnPool) planCleanup(conn *openConnWrapper) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		select {
		case <-time.After(p.idleTimeout):
			p.cleanup(ctx, conn)
		case <-ctx.Done():
		}
	}()

	return cancel
}

func (p *ConnPool) cleanup(ctx context.Context, conn *openConnWrapper) {
	p.mu.Lock()
	defer p.mu.Unlock()

	select {
	case <-ctx.Done():
	default:
		for key, list := range p.openConns {
			for index, connection := range list {
				if connection != conn {
					continue
				}

				err := conn.conn.Close()
				if err != nil {
					panic(fmt.Errorf("failed to close connection: %w", err))
				}

				p.openConns[key] = append(
					p.openConns[key][:index],
					p.openConns[key][index+1:]...,
				)

				if len(p.openConns[key]) == 0 {
					delete(p.openConns, key)
				}

				p.numOpenConns--

				for len(p.waitingQueue) != 0 {
					req := p.waitingQueue[0]
					p.waitingQueue = p.waitingQueue[1:]

					go p.dial(req)
				}

				return
			}
		}

		panic("try to close unknown connection")
	}
}
